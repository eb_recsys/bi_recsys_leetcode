# coding: UTF-8
__author__ = 'LiMingji'


class RemoveInvalidParentheses(object):
    def removeInvalidParentheses(self, s):
        def dfs(s, visited):
            m = calc(s)
            if m == 0:
                res.append(s)
            else:
                for i in xrange(len(s)):
                    if s[i] in '()':
                        new_s = s[:i] + s[i + 1:]
                        if new_s not in visited and calc(new_s) < m:
                            visited.append(new_s)
                            dfs(new_s, visited)

        def calc(s):
            a, b = 0, 0
            for x in s:
                # 这里a相当于从左到右加上所有的所右括号，如果其大于或者等于0，那么肯定没有invalid括号
                # 如果a等于0，那么肯定没有invalid
                # 如果a大于0，例如'()(',这里a就记录了多出来的左括号个数
                # 如果a小于0，例如‘))(’,这里a首先是负数，然后让b来记录负数，负数为invalid右括号个数。
                # 所以这里a应该是记录invalid左括号个数，b记录invalid右括号个数
                a += {'(': 1, ')': -1}.get(x, 0)
                b += a < 0
                a = max(a, 0)
            return a + b

        res = []
        dfs(s, [])
        return res


m = RemoveInvalidParentheses()

print m.removeInvalidParentheses(s="()())()")
