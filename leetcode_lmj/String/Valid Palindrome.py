__author__ = 'LiMingji'


class ValidePalindrome(object):
    def isPalindrome(self, s):
        s = s.lower()
        left = 0
        right = len(s) - 1
        while left < right:
            if not (ord('a') <= ord(s[left]) <= ord('z') or ord('0') <= ord(s[left]) <= ord('9')):
                left += 1
                continue
            if not (ord('a') <= ord(s[right]) <= ord('z') or ord('0') <= ord(s[right]) <= ord('9')):
                right -= 1
                continue
            if s[left] == s[right]:
                left += 1
                right -= 1
            else:
                return False
        return True


input = "A man, a plan, a canal: Panama"
# input = "ce a c"
input = "0P"
print ValidePalindrome().isPalindrome(input)
