class ReverseVowelsOfAString(object):
    def reverseVowels(self, s):
        vowels = ['a', 'o', 'e', 'i', 'u', 'A', 'O', 'E', 'I', 'U']
        ret = list(s)

        left = 0
        right = len(s) - 1
        while left < right:
            while left < right and ret[left] not in vowels:
                left += 1
            while left < right and ret[right] not in vowels:
                right -= 1
            if left < right:
                ret[left], ret[right] = ret[right], ret[left]
                left += 1
                right -= 1
        return ''.join(ret)


input_str = "leetcode"
print ReverseVowelsOfAString().reverseVowels(input_str)
