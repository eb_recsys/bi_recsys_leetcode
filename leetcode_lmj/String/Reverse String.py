class ReverseString(object):
    def reverseString(self, s):
        return s[-1: -len(s)-1: -1]


input = "hello"
print ReverseString().reverseString(input)
