class TrieTreeNode(object):
    def __init__(self):
        self.val = 'root'
        self.children = [None] * 1000
        self.isEndNode = False
        self.count = 0


class TrieTree(object):
    def __init__(self):
        self.root = TrieTreeNode()

    def add_word(self, word):
        cur_node = self.root
        for x in word:
            if cur_node.children[ord(x) - 0] is None:
                new_node = TrieTreeNode()
                new_node.val = x
                cur_node.children[ord(x) - 0] = new_node
            cur_node.count += 1
            cur_node = cur_node.children[ord(x) - 0]
        cur_node.isEndNode = True

    def count_prefix(self, word):
        cur_node = self.root
        for x in word:
            if cur_node.children[ord(x) - 0] is not None:
                cur_node = cur_node.children[ord(x) - 0]
            else:
                return 0
        if cur_node.isEndNode is True:
            val = cur_node.count - 1
            if val <= 0:
                return 0
            else:
                return val
        else:
            return cur_node.count


# while True:
#     try:
#         input_nums = int(raw_input())
#         root = TrieTree()
#         for i in range(0, input_nums):
#             input_stt = raw_input()
#             root.add_word(input_stt)
#         test_nums = int(raw_input())
#         for i in range(0, test_nums):
#             test_str = raw_input()
#             print root.count_prefix(test_str)
#     except EOFError:
#         break

root = TrieTree()
root.add_word("babaab")
root.add_word("babbbaaaa")
root.add_word("abba")
root.add_word("aaaaabaa")
root.add_word("babaababb")

print root.count_prefix("abba")

# print root.count_prefix("babb")
# print root.count_prefix("baabaaa")
# print root.count_prefix("bab")
# print root.count_prefix("bb")
# print root.count_prefix("bbabbaab")
