# coding : UTF-8
__author__ = 'LiMingji'


class BinarySearch(object):
    """
        普通二分查找
    """

    @staticmethod
    def binary_search(nums, target):
        low = 0
        high = len(nums) - 1
        ans = -1
        while low <= high:
            mid = (low + high) / 2
            if nums[mid] <= target:
                low = mid + 1
                if nums[mid] == target:
                    ans = mid
                    break
            else:
                high = mid - 1
        return ans

    """
        二分查找: 求最小的i使得 nums[i] == target 不存在返回-1
    """

    @staticmethod
    def binary_search_min(nums, target):
        low = 0
        high = len(nums) - 1
        ans = -1
        while low <= high:
            mid = (low + high) / 2
            if nums[mid] < target:
                low = mid + 1
            else:
                high = mid - 1
                if nums[mid] == target:
                    ans = mid
        return ans

    """
        二分查找: 求最大的i使得 nums[i] == target 不存在返回-1
    """

    @staticmethod
    def binary_search_max(nums, target):
        low = 0
        high = len(nums) - 1
        ans = -1
        while low <= high:
            mid = (low + high) / 2
            if nums[mid] <= target:
                low = mid + 1
                if nums[mid] == target:
                    ans = mid
            else:
                high = mid - 1
        return ans

    """
        二分查找: 求最大的i使得 nums[i]小于 target，不存在返回-1；
    """

    @staticmethod
    def binary_search_less_max(nums, target):
        low = 0
        high = len(nums) - 1
        ans = -1

        while low <= high:
            mid = (low + high) / 2
            if nums[mid] < target:
                low = mid + 1
                ans = mid
            else:
                high = mid - 1
        return ans

    """
        二分查找: 求最小的i使得 nums[i]大于 target，不存在返回-1；
    """

    @staticmethod
    def binary_search_bigger_min(nums, target):
        low = 0
        high = len(nums) - 1
        ans = -1

        while low <= high:
            mid = (low + high) / 2
            if nums[mid] <= target:
                low = mid + 1
            else:
                high = mid - 1
                ans = mid
        return ans


input_nums = [1, 2, 3, 4, 5, 6]
# print BinarySearch.search(input_nums, 3)
print BinarySearch.binary_search(input_nums, 3)
