class HeapSort(object):
    def adjust(self, inputs, pos):
        min_pos = pos
        left = pos * 2 + 1
        if left < len(inputs) and inputs[left] <= inputs[min_pos]:
            min_pos = left
        right = pos * 2 + 2
        if right < len(inputs) and inputs[right] <= inputs[min_pos]:
            min_pos = right
        if min_pos == pos:
            return
        else:
            inputs[pos], inputs[min_pos] = inputs[min_pos], inputs[pos]
            self.adjust(inputs, min_pos)

    def build_heap(self, inputs):
        for i in range(len(inputs) / 2, -1, -1):
            self.adjust(inputs, i)

    def heap_sort(self, inputs):
        self.build_heap(inputs)
        ret = []
        while len(inputs) > 0:
            ret.append(inputs[0])
            inputs[0], inputs[len(inputs) - 1] = inputs[len(inputs) - 1], inputs[0]
            inputs.pop(len(inputs) - 1)
            self.adjust(inputs, 0)
        return ret


nums = [5, 2, 1, 4, 6, 1, 1, 2, 3, 4, 5]
# nums = [3, 5, 1, 4, 2]
print HeapSort().heap_sort(nums)
