# coding : UTF-8
__author__ = 'LiMingji'

'''
http://blog.csdn.net/wumuzi520/article/details/8078322
'''


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class QuickSortWithLinkedList(object):
    def quick_sort_partition(self, head, tail):
        if head is tail:
            return head
        key = head.val
        p = head
        q = head.next
        while q is not tail:
            if q.val < key:
                p = p.next
                p.val, q.val = q.val, p.val
            q = q.next
        head.val, p.val = p.val, head.val
        self.quick_sort_partition(head, p)
        self.quick_sort_partition(p.next, tail)
        return head

    def quick_sort(self, head):
        return self.quick_sort_partition(head, None)


def print_list(head):
    while head is not None:
        print str(head.val) + "==>",
        head = head.next


def init_list(num):
    dummy = cur = ListNode(-1)
    for x in num:
        cur.next = ListNode(x)
        cur = cur.next
    return dummy.next


list_input = [3, 3, 4, 2, 1, 3]
s = QuickSortWithLinkedList()
print_list(s.quick_sort(init_list(list_input)))
