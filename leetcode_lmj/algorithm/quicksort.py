# coding: UTF-8
__author__ = 'LiMingji'


def sort(num, start, end):
    if start >= end:
        return
    low = start
    high = end
    key = num[low]
    while low < high:
        while low < high and num[high] >= key:
            high -= 1
        num[low] = num[high]
        while low < high and nums[low] <= key:
            low += 1
        nums[high] = num[low]
    nums[low] = key
    sort(num, start, low - 1)
    sort(num, low + 1, end)


def quick_sort(num):
    sort(num, 0, len(num) - 1)


nums = [5, 2, 1, 4, 6, 1, 1, 2, 3, 4, 5]
quick_sort(nums)
print nums
