# coding: UTF-8

__author__ = 'LiMingji'


class Subsets(object):
    # """
    # :type nums: List[int]
    # :rtype: List[List[int]]
    # """
    def dfs(self, nums, step, cur, ret):
        if step > len(nums):
            return
        elif step == len(nums):
            tmp = []
            for x in cur:
                tmp.append(x)
            ret.append(tmp)
        elif step < len(nums):
            cur.append(nums[step])
            self.dfs(nums, step + 1, cur, ret)
            cur.remove(nums[step])
            self.dfs(nums, step + 1, cur, ret)

    def subsets(self, nums):
        ret = []
        cur = []
        self.dfs(sorted(nums), 0, cur, ret)
        return ret

nums = (3, 2, 1)
s = Subsets()
ret = s.subsets(nums)
print ret
