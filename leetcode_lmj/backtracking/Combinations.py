# coding: UTF-8

__author__ = 'LiMingji'


class Combinations(object):
    def dfs(self, step, n, k, cur, ret):
        if len(cur) == k:
            temp = []
            for x in cur:
                temp.append(x)
            ret.append(temp)
        else:
            for i in range(step+1, n+1):
                cur.append(i)
                self.dfs(i, n, k, cur, ret)
                cur.remove(i)

    def combine(self, n, k):
        ret_input = []
        cur = []
        self.dfs(0, n, k, cur, ret_input)
        return ret_input
        

s = Combinations()
rets = s.combine(4, 2)
print rets
