import copy


class CombinationSumIII(object):
    def dfs(self, candidates, target, cur_pos, cur, ret):
        if target == 0:
            ret.append(copy.copy(cur))
            return
        if cur_pos >= len(candidates):
            return
        if candidates[cur_pos] > target:
            return
        else:
            index = cur_pos
            while index < len(candidates):
                cur.append(candidates[index])
                self.dfs(candidates, target - candidates[index], index + 1, cur, ret)
                cur.pop(len(cur) - 1)

                while index < len(candidates) - 1 and candidates[index] == candidates[index + 1]:
                    index += 1
                index += 1

    def combinationSum2(self, candidates, target):
        ret = []
        cur = []
        if len(candidates) == 0:
            return ret
        candidates.sort()
        self.dfs(candidates, target, 0, cur, ret)
        return ret


m = CombinationSumIII()
print m.combinationSum2(candidates=[1, 1, 2, 5, 6, 7, 10], target=8)
# print m.combinationSum2(candidates=[1, 2, 5], target=8)
