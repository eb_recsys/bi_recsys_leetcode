# coding: UTF-8

__author__ = 'LiMingji'


class Permutations(object):

    def dfs(self, nums, cur, ret):
        if len(cur) == len(nums):
            temp = []
            for x in cur:
                temp.append(x)
            ret.append(temp)
        for x in nums:
            if x not in cur:
                cur.append(x)
                self.dfs(nums, cur, ret)
                cur.remove(x)

    def permute(self, nums):
        ret = []
        cur = []
        self.dfs(nums, cur, ret)
        return ret


nums = (1, 2, 3)
s = Permutations()
rets = s.permute(sorted(nums))
print rets