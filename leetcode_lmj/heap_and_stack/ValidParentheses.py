class ValidParentheses(object):
    def isValid(self, s):
        stack = []

        for elem in s:
            if elem == "(" or elem == "[" or elem == "{":
                stack.append(elem)
            else:
                if len(stack) == 0:
                    return False
                top = stack.pop()
                if elem == ")" and top != "(":
                    return False
                elif elem == "]" and top != "[":
                    return False
                elif elem == "}" and top != "{":
                    return False

        return True if len(stack) == 0 else False


m = ValidParentheses()

print m.isValid("(]")
