import heapq


class SuperUglyNumber(object):
    def nthSuperUglyNumber(self, n, primes=[]):
        length = len(primes)
        idx = [0] * length
        ans = [1]
        minlist = [(primes[i] * ans[idx[i]], i) for i in xrange(len(idx))]
        heapq.heapify(minlist)
        while len(ans) < n:
            (umin, min_idx) = heapq.heappop(minlist)
            idx[min_idx] += 1
            if umin != ans[-1]:
                ans.append(umin)
            heapq.heappush(minlist, (primes[min_idx] * ans[idx[min_idx]], min_idx))

        return ans[-1]


input_primes = [2, 7, 13, 19]
print SuperUglyNumber().nthSuperUglyNumber(12, input_primes)
