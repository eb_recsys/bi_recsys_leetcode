__author__ = 'LiMingji'


class PlusOne(object):
    def plusOne(self, digits):
        bit = 1
        ret = [0] * len(digits)
        index = len(digits) - 1
        while index >= 0:
            cur = digits[index] + bit
            bit = (digits[index] + bit) / 10
            cur %= 10
            ret[index] = cur
            index -= 1
        if bit == 1:
            ret.insert(0, 1)
        return ret


input_digits = [1, 9, 9, 9]
print PlusOne().plusOne(input_digits)
