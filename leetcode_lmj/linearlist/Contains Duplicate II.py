def containsNearbyDuplicate(nums, k):
    if len(nums) == 0 or k == 0:
        return False
    i = 0
    map = {}
    j = i + k - 1
    cur = i
    while cur <= j and cur < len(nums):
        if nums[cur] in map:
            return True
        else:
            map[nums[cur]] = True
        cur += 1

    left = i
    right = j + 1
    while right < len(nums):
        if nums[right] in map:
            return True
        map.pop(nums[left])
        map[nums[right]] = True
        left += 1
        right += 1
    return False


nums = [1, 2, 1]
k = 0

print(containsNearbyDuplicate(nums, k))
