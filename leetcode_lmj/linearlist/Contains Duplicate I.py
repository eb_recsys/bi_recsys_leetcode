# https://leetcode.com/problems/contains-duplicate/


def containsDuplicate(nums):
    map = {}
    for num in nums:
        if num in map:
            return True
        else:
            map[num] = True
    return False

print(containsDuplicate([1, 2, 3, 1]))
