class RemoveElement(object):
    def removeElement(self, nums, val):
        if len(nums) == 0:
            return 0
        left = 0
        right = len(nums) - 1

        while left < right:
            while nums[left] != val and left < right:
                left += 1
            while nums[right] == val and left < right:
                right -= 1

            nums[left], nums[right] = nums[right], nums[left]
            left += 1
            right -= 1

        index = 0
        while index < len(nums) and nums[index] != val:
            index += 1
        return index


m = RemoveElement()

print m.removeElement(nums=[3, 2, 2, 3], val=3)
print m.removeElement(nums=[3], val=2)
