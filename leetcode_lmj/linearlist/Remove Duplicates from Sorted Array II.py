# coding: UTF-8

__author__ = 'LiMingji'

'''
思路：
    和 RemoveDupulicatedsfromSotedArray 如出一辙。做法一样，这个稍微巧妙一些。
'''


class RemoveDuplicatesfromSortedArrayII(object):
    def removeDuplicates(self, nums):
        if len(nums) <= 2:
            return len(nums)
        first = 0
        end = 2
        while end < len(nums):
            if nums[end] != nums[first]:
                nums[first + 2] = nums[end]
                first += 1
            end += 1
        return first+2

nums = [1, 1, 1, 2, 2, 3]
# nums = [1, 1, 2, 3]
s = RemoveDuplicatesfromSortedArrayII()
print s.removeDuplicates(nums)
print nums
