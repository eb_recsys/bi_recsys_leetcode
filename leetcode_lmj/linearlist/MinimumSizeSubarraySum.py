class MinimumSizeSubarraySum(object):
    def minSubArrayLen(self, s, nums):
        if len(nums) == 0 or sum(nums) < s:
            return 0
        left = right = 0
        cur_sum = 0
        min_len = len(nums)

        while cur_sum < s and right < len(nums):
            while cur_sum < s and right < len(nums):
                cur_sum += nums[right]
                right += 1

            while cur_sum >= s and left < right:
                if cur_sum >= s:
                    min_len = min(min_len, right - left)
                cur_sum -= nums[left]
                left += 1

        return min_len


m = MinimumSizeSubarraySum()
print m.minSubArrayLen(s=7, nums=[2, 3, 1, 2, 4, 3])
