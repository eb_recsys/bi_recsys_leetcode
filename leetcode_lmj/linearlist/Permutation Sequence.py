__author__ = 'LiMingji'


class PermutationSequence(object):
    def getPermutation(self, n, k):
        base = 1
        nums = []
        for x in range(1, n + 1):
            nums.append(x)
            base *= x
        ret = []
        base /= n
        cur = n - 1
        k -= 1
        while cur > 0:
            ret.append(nums[k/base])
            nums.remove(nums[k/base])
            print k
            k %= base
            base /= cur
            cur -= 1
        ret.append(nums[0])
        return ret

print PermutationSequence().getPermutation(3, 5)
# for x in range(1, 7):
#     print PermutationSequence().getPermutation(3, x)

