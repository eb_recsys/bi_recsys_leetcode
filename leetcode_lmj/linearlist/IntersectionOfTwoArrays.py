class IntersectionOfTwoArrays(object):
    def intersection(self, nums1, nums2):
        nums_map = {}
        for x in nums1:
            nums_map[x] = True
        ret = []

        for x in nums2:
            if x in nums_map:
                ret.append(x)
                nums_map.pop(x)
        return ret


m = IntersectionOfTwoArrays()

print m.intersection(nums1=[1, 2, 2, 1], nums2=[2, 2])
