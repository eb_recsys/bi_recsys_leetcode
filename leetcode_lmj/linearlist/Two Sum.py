__author__ = 'LiMingji'


class TwoSum(object):
    def twoSum(self, nums, target):
        nums_map = {}
        index = 0
        while index < len(nums):
            nums_map[nums[index]] = index
            index += 1

        index = 0
        while index < len(nums):
            if target-nums[index] in nums_map and index < nums_map[target-nums[index]]:
                return index, nums_map[target-nums[index]]
            index += 1

# nums = [2, 7, 11, 15]
nums = [3, 2, 4]
s = TwoSum()
print s.twoSum(nums, 6)
