__author__ = 'LiMingji'


class SetMatrixZeroes(object):
    def setZeroes(self, matrix):
        row_size = len(matrix)
        col_size = len(matrix[0])
        row_has_zero = False
        col_has_zero = False

        for i in range(0, row_size):
            if matrix[i][0] == 0:
                col_has_zero = True
                break
        for j in range(0, col_size):
            if matrix[0][j] == 0:
                row_has_zero = True
                break

        for i in range(1, row_size):
            for j in range(1, col_size):
                if matrix[i][j] == 0:
                    matrix[i][0] = 0
                    matrix[0][j] = 0

        print matrix

        for i in range(1, row_size):
            for j in range(1, col_size):
                if matrix[i][0] == 0 or matrix[0][j] == 0:
                    matrix[i][j] = 0

        if row_has_zero:
            for j in range(0, col_size):
                matrix[0][j] = 0
        if col_has_zero:
            for i in range(0, row_size):
                matrix[i][0] = 0


input_matrix = [[2147483647, 2, 9], [2, 6, 7], [1, 8, 8], [5, 0, 1], [9, 6, 0]]
SetMatrixZeroes().setZeroes(input_matrix)
print input_matrix
