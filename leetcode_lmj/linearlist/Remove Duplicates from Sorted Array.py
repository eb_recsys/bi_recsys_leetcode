# coding: UTF-8

__author__ = 'LiMingji'


class RemoveDuplicatesfromSortedArray(object):
    def removeDuplicates(self, nums):
        if len(nums) <= 1:
            return len(nums)
        first = 0
        end = 1
        while end < len(nums):
            if nums[end] != nums[first]:
                first += 1
                nums[first] = nums[end]
            end += 1
        return first+1

nums = [1, 2, 2]
s = RemoveDuplicatesfromSortedArray()
print s.removeDuplicates(nums)
print nums
