__author__ = 'LiMingji'


class SearchInRotatedSortedArrayII(object):
    def search(self, nums, target):
        left = 0
        right = len(nums)-1
        while left <= right:
            mid = left + (right - left)/2
            if nums[mid] == target:
                return True
            elif nums[left] == nums[right] and not nums[left] == target:
                left += 1
                right -= 1
            elif nums[left] <= target < nums[mid]:
                right = mid - 1
            elif nums[left] <= nums[mid] and not (nums[left] <= target < nums[mid]):
                left = mid + 1
            elif nums[mid] <= target <= nums[right]:
                left = mid + 1
            elif nums[mid] <= nums[right] and not (nums[mid] <= target <= nums[right]):
                right = mid - 1
        return False

# input_nums = [4, 5, 6, 7, 0, 1, 2, 3, 4, 4, 4, 4, 4, 4]
input_nums = [1, 3, 1, 1, 1]
# input_nums = [3, 1]
# input_nums = [1, 2, 1]
print SearchInRotatedSortedArrayII().search(input_nums, 1)
