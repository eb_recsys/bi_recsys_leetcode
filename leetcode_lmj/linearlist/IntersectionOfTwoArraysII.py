class IntersectionOfTwoArraysII(object):
    def intersect(self, nums1, nums2):
        nums_map = {}
        for x in nums1:
            if x in nums_map:
                nums_map[x] += 1
            else:
                nums_map[x] = 1

        ret = []
        for x in nums2:
            if x in nums_map and nums_map[x] > 0:
                ret.append(x)
                nums_map[x] -= 1
        return ret


m = IntersectionOfTwoArraysII()

print m.intersect(nums1=[1, 2, 2, 1], nums2=[2, 2])
