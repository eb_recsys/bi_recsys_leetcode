__author__ = 'LiMingji'


class TrappingRainWater(object):
    def trap(self, height):
        left_max = [0] * len(height)
        right_max = [0] * len(height)
        water = [0] * len(height)

        index = 1
        while index < len(height):
            left_max[index] = max(left_max[index - 1], height[index - 1])
            index += 1

        index = len(height) - 2
        while index >= 0:
            right_max[index] = max(right_max[index + 1], height[index + 1])
            index -= 1

        index = 0
        while index < len(height):
            water[index] = max(min(left_max[index], right_max[index]) - height[index], 0)
            index += 1
        return sum(water)


given = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
print TrappingRainWater().trap(given)
