__author__ = 'LiMingji'


class MedianOfTwoSortedArrays(object):
    def findKthElem(self, nums1, nums2, k):
        if len(nums1) < len(nums2):
            return self.findKthElem(nums2, nums1, k)
        if len(nums2) is 0:
            return nums1[k-1]
        if k == 1:
            return min(nums1[0], nums2[0])
        pos = min(k / 2, len(nums2)) - 1
        if nums1[pos] < nums2[pos]:
            return self.findKthElem(nums1[pos+1:], nums2, k - pos - 1)
        else:
            return self.findKthElem(nums1, nums2[pos+1:], k - pos - 1)

    def findMedianSortedArrays(self, nums1, nums2):
        total_len = len(nums1) + len(nums2)
        if total_len % 2 == 1:
            return self.findKthElem(nums1, nums2, (total_len + 1) / 2)
        else:
            return float(self.findKthElem(nums1, nums2, (total_len + 1) / 2) +
                     self.findKthElem(nums1, nums2, (total_len + 1) / 2 + 1)) / 2


input1 = [1, 3, 5, 7, 9, 11]
input2 = [2, 4, 6, 8, 10, 12]
s = MedianOfTwoSortedArrays()

print s.findMedianSortedArrays(input1, input2)


# for x in range(1, 11):
#     print s.findKthElem(input1, input2, x)


