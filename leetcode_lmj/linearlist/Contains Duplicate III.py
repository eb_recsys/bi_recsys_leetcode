import collections


class ContainsDuplicateIII(object):
    def containsNearbyAlmostDuplicate(self, nums, k, t):
        """
        :type nums: List[int]
        :type k: int
        :type t: int
        :rtype: bool
        """
        if k < 1 or t < 0:
            return False

        num_dict = collections.OrderedDict()
        for x in range(len(nums)):
            key = nums[x] / max(1, t)
            for m in (key, key - 1, key + 1):
                if m in num_dict and abs(nums[x] - num_dict[m]) <= t:
                    return True
            num_dict[key] = nums[x]
            if x >= k:
                num_dict.popitem(last=False)
        return False


m = ContainsDuplicateIII()

# print(containsNearbyAlmostDuplicate([3, 6, 0, 2], 2, 2))
# print(containsNearbyAlmostDuplicate([3, 5, 1, 4, 2, 5], 1, 1))
# print(containsNearbyAlmostDuplicate([-1, -1], 1, 0))
# print(containsNearbyAlmostDuplicate([-1, 2147483647], 1, 2147483647))
# print(containsNearbyAlmostDuplicate([1, 3, 1], 1, 1))
# print(containsNearbyAlmostDuplicate([-3, 3], 2, 4))  # false
# print(containsNearbyAlmostDuplicate([-1, -1], 1, 0))
print(m.containsNearbyAlmostDuplicate([3, 6, 0, 4], 2, 2))
