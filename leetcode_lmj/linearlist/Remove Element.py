__author__ = 'LiMingji'


class RemoveElement(object):
    def removeElement(self, nums, val):
        left = 0
        right = len(nums) - 1
        while left < right:
            while left < right and nums[right] == val:
                right -= 1
            while left < right and nums[left] != val:
                left += 1
            nums[left], nums[right] = nums[right], nums[left]
        if right == left == 0 and nums[right] == val:
            return 0
        return right + 1

# input_nums = [3, 2, 2, 3]
input_nums = [1, 1]
s = RemoveElement()
print s.removeElement(input_nums, 2)
print input_nums
