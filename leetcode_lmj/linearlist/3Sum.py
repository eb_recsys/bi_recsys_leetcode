__author__ = 'LiMingji'


class ThreeSum(object):
    def threeSum(self, nums):
        size = len(nums)
        ret = []
        first = 0
        sorted_input = sorted(nums)
        ret_set = set()
        while first < size / 2:
            left = first + 1
            right = size - 1
            while left < right:
                cur = sorted_input[first] + sorted_input[left] + sorted_input[right]
                if cur is 0:
                    ret_set.add((sorted_input[first], sorted_input[left], sorted_input[right]))
                    right -= 1
                    left += 1
                elif cur > 0:
                    right -= 1
                elif cur < 0:
                    left += 1
            first += 1

        for x in ret_set:
            ret.append(list(x))
        return ret


nums = [-1, 0, 1, 2, -1, -4]
s = ThreeSum()
print s.threeSum(nums)
