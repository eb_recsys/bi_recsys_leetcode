__author__ = 'LiMingji'


class GasStation(object):
    def canCompleteCircuit(self, gas, cost):
        total = 0
        pos = -1
        index = 0
        sum = 0
        while index < len(gas):
            sum += gas[index] - cost[index]
            total += gas[index] - cost[index]
            if sum < 0:
                pos = index
                sum = 0
            index += 1
        return pos + 1 if total > 0 else -1


nums_gas = [1, 3, 4, 2, 5]
nums_cost = [4, 4, 1, 3, 2]
print GasStation().canCompleteCircuit(nums_gas, nums_cost)
