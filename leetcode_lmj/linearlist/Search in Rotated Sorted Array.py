# coding: UTF-8
__author__ = 'LiMingji'

'''
细节题，分情况讨论。注意边界
'''


class SearchinRotatedSortedArray(object):
    def search(self, nums, target):
        left = 0
        right = len(nums)-1
        while left <= right:
            mid = (left + right)/2
            if nums[mid] == target:
                return mid
            elif nums[left] <= nums[mid] and nums[left] <= target < nums[mid]:
                right = mid - 1
            elif nums[left] <= nums[mid] and not nums[left] < target < nums[mid]:
                left = mid + 1
            elif nums[mid] <= nums[right] and nums[mid] < target <= nums[right]:
                left = mid + 1
            elif nums[mid] <= nums[right] and not nums[mid] < target < nums[right]:
                right = mid - 1
        return -1


nums = [4, 5, 6, 7, 0, 1, 2]
s = SearchinRotatedSortedArray()
print s.search(nums, 0)
