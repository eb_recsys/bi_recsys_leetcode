__author__ = 'LiMingji'


class NextPermutation(object):
    def nextPermutation(self, nums):
        if len(nums) <= 1:
            return
        last = len(nums) - 1
        last_sec = len(nums) - 2

        if nums[last] > nums[last_sec]:
            nums[last], nums[last_sec] = nums[last_sec], nums[last]
            return
        else:
            start = last_sec
            while start >= 0 and nums[start] >= nums[start+1]:
                start -= 1
            if start < 0:
                nums[:] = sorted(nums[:])
            else:
                pos = len(nums)-1
                while pos > start:
                    if nums[pos] > nums[start]:
                        break
                    pos -= 1
                nums[start], nums[pos] = nums[pos], nums[start]
                nums[start+1:] = sorted(nums[start+1:])

input_nums = [1, 4, 3, 2]
input_nums = [1, 2, 3]
input_nums = [2, 3, 1]
input_nums = [1, 5, 1]
input_nums = [5, 4, 7, 5, 3, 2]

s = NextPermutation()
s.nextPermutation(input_nums)
print input_nums
