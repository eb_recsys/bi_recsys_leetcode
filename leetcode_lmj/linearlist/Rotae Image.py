# encoding:UTF-8
__author__ = 'LiMingji'

'''
对折，对角线互换
'''


class RotateImage(object):
    def rotate(self, matrix):
        size = len(matrix)
        index = 0
        while index < size - 1 - index:
            matrix[index], matrix[size - 1 - index] = matrix[size - 1 - index], matrix[index]
            index += 1

        i = 0
        while i < size:
            j = i + 1
            while j < size:
                matrix[i][j], matrix[j][i] = matrix[j][i], matrix[i][j]
                j += 1
            i += 1


input_matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
RotateImage().rotate(input_matrix)
print input_matrix
