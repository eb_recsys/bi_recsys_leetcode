__author__ = 'LiMingji'


class LongestConsecutiveSequence(object):
    def longestConsecutive(self, nums):
        nums_set = set()
        for x in nums:
            nums_set.add(x)
        max_len = 0
        for x in nums:
            if x not in nums_set:
                continue
            cur_len = 1
            low = x - 1
            while low in nums_set:
                nums_set.remove(low)
                cur_len += 1
                low -= 1
            high = x + 1
            while high in nums_set:
                nums_set.remove(high)
                cur_len += 1
                high += 1
            max_len = max(max_len, cur_len)
        return max_len


input_nums = [100, 4, 200, 1, 3, 2]
s = LongestConsecutiveSequence()

print s.longestConsecutive(input_nums)
