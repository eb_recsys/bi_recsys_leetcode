__author__ = 'LiMingji'

import sys


class ThreeSumClosest(object):
    def threeSumClosest(self, nums, target):
        sorted_nums = sorted(nums)
        index = 0
        min_gap = sys.maxint
        ret = 0
        while index < len(nums):
            left = index + 1
            right = len(nums)-1
            while left < right:
                cur = sorted_nums[index] + sorted_nums[left] + sorted_nums[right]
                if min_gap > abs(cur - target):
                    min_gap = abs(cur - target)
                    ret = cur
                if cur == target:
                    return target
                elif cur > target:
                    right -= 1
                elif cur < target:
                    left += 1
            index += 1
        return ret


input_nums = [-1, 2, 1, -4]
s = ThreeSumClosest()
print s.threeSumClosest(input_nums, 1)

