__author__ = 'LiMingji'

'''
http://www.cnblogs.com/felixfang/p/3620086.html
'''


class Candy(object):
    def candy(self, ratings):
        ret = [1] * len(ratings)
        # from left to right
        index = 0
        while index < len(ratings) - 1:
            if ratings[index] > ratings[index + 1] and ret[index] <= ret[index + 1]:
                ret[index] = ret[index + 1] + 1
            elif ratings[index] < ratings[index + 1] and ret[index] >= ret[index + 1]:
                ret[index + 1] = ret[index] + 1
            index += 1
        # from right to left
        index = len(ratings) - 1
        while index > 0:
            if ratings[index] > ratings[index - 1] and ret[index] <= ret[index - 1]:
                ret[index] = ret[index - 1] + 1
            elif ratings[index] < ratings[index - 1] and ret[index] >= ret[index - 1]:
                ret[index - 1] = ret[index] + 1
            index -= 1
        return sum(ret)


nums = [2, 2, 2, 1, 3]
print Candy().candy(nums)
