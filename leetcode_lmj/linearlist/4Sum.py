__author__ = 'LiMingji'


class FourSum(object):
    def fourSum(self, nums, target):
        sorted_nums = sorted(nums)
        ret = []
        first = 0
        while first < len(nums):
            second = first + 1
            while second < len(nums):
                left = second + 1
                right = len(nums) - 1
                while left < right:
                    val = sorted_nums[first] + sorted_nums[second] + sorted_nums[left] + sorted_nums[right]
                    if val == target:
                        cur = [sorted_nums[first], sorted_nums[second], sorted_nums[left], sorted_nums[right]]
                        if cur not in ret:
                            ret.append(cur)
                        left += 1
                        right -= 1
                    elif val < target:
                        left += 1
                    elif val > target:
                        right -= 1
                second += 1
            first += 1
        return ret

input_nums = [1, 0, -1, 0, -2, 2]
s = FourSum()
print s.fourSum(input_nums, 0)



