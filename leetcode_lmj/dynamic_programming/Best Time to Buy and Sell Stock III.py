__author__ = 'LiMingji'

'''
http://www.cnblogs.com/lihaozy/archive/2012/12/19/2825525.html
'''


class BestTimeToBuyAndSellStockIII(object):
    def maxProfit(self, prices):
        if len(prices) <= 1:
            return 0
        forward = [0 for elem in range(len(prices))]
        back = [0 for elem in range(len(prices))]

        cur_min = prices[0]
        index = 1
        while index < len(prices):
            cur_min = min(cur_min, prices[index])
            forward[index] = max(forward[index - 1], prices[index] - cur_min)
            index += 1

        ret = 0
        index = len(prices) - 2
        cur_max = prices[-1]
        while index >= 0:
            cur_max = max(cur_max, prices[index])
            back[index] = min(back[index + 1], prices[index] - cur_max)
            ret = max(ret, forward[index] - back[index])
            index -= 1
        return ret


input_list = [3, 1, 4, 2, 5, 6]
# input_list = [3, 1]
s = BestTimeToBuyAndSellStockIII()
print s.maxProfit(input_list)
