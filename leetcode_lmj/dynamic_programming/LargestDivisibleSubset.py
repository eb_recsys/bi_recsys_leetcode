class LargestDivisibleSubset(object):
    def largestDivisibleSubset(self, nums):
        ret = []
        if len(nums) == 0:
            return ret

        nums.sort(reverse=True)
        dp = [1 for x in range(len(nums))]
        pre = [-1 for x in range(len(nums))]

        for right in range(0, len(nums)):
            for left in range(0, right):
                if nums[left] % nums[right] == 0 and dp[left] + 1 > dp[right]:
                    dp[right] = dp[left] + 1
                    pre[right] = left
        max_pos = 0
        for index in range(len(dp)):
            if dp[max_pos] < dp[index]:
                max_pos = index

        print dp

        pre_pos = max_pos
        while pre_pos != -1:
            ret.append(nums[pre_pos])
            pre_pos = pre[pre_pos]
        return ret


m = LargestDivisibleSubset()
# nums = [1, 2, 4, 8, 3, 7]
nums = [16, 8, 4, 3]
# nums = [1, 2, 3]
# nums = [1, 2, 4, 3, 7]

print m.largestDivisibleSubset(nums)
