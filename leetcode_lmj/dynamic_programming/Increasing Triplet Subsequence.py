import sys


class IncreasingTripletSubsequence(object):
    def increasingTriplet(self, nums):
        if len(nums) < 3:
            return False
        first_min, sec_min = sys.maxint, sys.maxint
        for i in range(0, len(nums)):
            if nums[i] > sec_min:
                return True
            if sec_min > nums[i] > first_min:
                sec_min = nums[i]
            elif first_min > nums[i]:
                first_min = nums[i]
        return False


input_nums = [5, 1, 5, 5, 2, 5, 4]
print IncreasingTripletSubsequence().increasingTriplet(input_nums)
