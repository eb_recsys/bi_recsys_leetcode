# coding: UTF-8
__author__ = 'LiMingji'

'''
    House Robber I的升级版. 因为第一个element 和最后一个element不能同时出现. 则分两次call House Robber I. case 1: 不包括最后一个element. case 2: 不包括第一个element.
两者的最大值即为全局最大值
'''


class HouseRobberII(object):
    def robsub(self, nums, start, end):
        dp = [0 for x in range(0, end + 1)]
        index = start
        while index <= end:
            yesterday = 0 if index == start else dp[index - 1]
            anteayer = 0 if index < start + 1 else dp[index - 2]
            dp[index] = max(nums[index] + anteayer, yesterday)
            index += 1
        return dp[end]

    def rob(self, nums):
        if len(nums) == 0:
            return 0
        elif len(nums) == 1:
            return nums[0]
        else:
            return max(self.robsub(nums, 0, len(nums) - 2),
                       self.robsub(nums, 1, len(nums) - 1))


input_nums = [3, 5, 1, 4, 2]
print HouseRobberII().rob(input_nums)
