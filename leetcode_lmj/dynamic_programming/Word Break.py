__author__ = 'LiMingji'


class WordBreak(object):
    def wordBreak(self, s, wordDict):
        if s in wordDict:
            return True
        dp = [0 for elem in range(len(s) + 1)]
        dp[0] = 1
        i = 1
        while i <= len(s):
            if dp[i - 1] == 1:
                j = i - 1
                index = i - 1
                while j < len(s):
                    if s[index: j + 1] in wordDict:
                        dp[j + 1] = 1
                    j += 1
            i += 1
        print dp
        return True if dp[-1] == 1 else False


input_str = "catsanddog"
dict = set()
dict.add("cat")
dict.add("sand")
dict.add("dog")

# input_str = "abcd"
# dict = set()
# dict.add("a")
# dict.add("abc")
# dict.add("b")
# dict.add("cd")

print WordBreak().wordBreak(input_str, dict)
