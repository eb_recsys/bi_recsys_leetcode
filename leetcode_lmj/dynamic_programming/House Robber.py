class HouseRobber(object):
    def rob(self, nums):
        dp = [0 for x in range(0, len(nums) + 1)]
        index = 0
        while index < len(nums):
            yesterday = 0 if index == 0 else dp[index - 1]
            anteayer = 0 if index < 1 else dp[index - 2]
            dp[index] = max(nums[index] + anteayer, yesterday)
            index += 1
        return dp[len(nums)-1]

input_nums = [3, 5, 1, 4, 2]
print HouseRobber().rob(input_nums)
