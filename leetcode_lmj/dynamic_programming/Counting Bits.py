class CountingBits(object):
    def countBits(self, num):
        dp = [0 for x in range(0, num + 1)]
        dp[0] = 0
        for x in range(1, num + 1):
            if x % 2 == 1:
                dp[x] = dp[x / 2] + 1
            else:
                dp[x] = dp[x / 2]
        return dp


print CountingBits().countBits(15)
# print bin(5).replace('0b', '')
