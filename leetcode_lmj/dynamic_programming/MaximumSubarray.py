class MaximumSubarray(object):
    def maxSubArray(self, nums):
        dp = [0 for x in range(0, len(nums))]
        dp[0] = nums[0]

        for index in range(1, len(nums)):
            dp[index] = max(nums[index], dp[index - 1] + nums[index])

        return max(dp)


m = MaximumSubarray()
# print m.maxSubArray(nums=[-2, 1, -3, 4, -1, 2, 1, -5, 4])
print m.maxSubArray(nums=[1])
