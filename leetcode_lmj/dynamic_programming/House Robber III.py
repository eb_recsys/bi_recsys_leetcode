# encode : UTF-8

from Tree import TreeNode

'''
dfs +　剪枝
'''


class HouseRobberIII(object):
    dicts = {}

    def benefit(self, root):
        if root is None:
            return 0

        if root in self.dicts:
            return self.dicts[root]

        left = 0 if root.left is None else self.benefit(root.left)
        right = 0 if root.right is None else self.benefit(root.right)

        left_left = 0 if root.left is None or root.left.left is None else self.benefit(root.left.left)
        left_right = 0 if root.left is None or root.left.right is None else self.benefit(root.left.right)

        right_left = 0 if root.right is None or root.right.left is None else self.benefit(root.right.left)
        right_right = 0 if root.right is None or root.right.right is None else self.benefit(root.right.right)

        self.dicts[root.left] = left
        self.dicts[root.right] = right
        # self.dicts[root] = max(left + right, root.val + left_left + left_right + right_left + right_right,
        #                        right + left_left + left_right, left + right_left + right_right)
        self.dicts[root] = max(left + right, root.val + left_left + left_right + right_left + right_right)
        return self.dicts[root]

    def rob(self, root):
        self.dicts = {}
        ret = self.benefit(root)
        return ret


# input_nums = [3, 2, 3, '#', 3, '#', 1]
# input_nums = [3, 4, 5, 1, 3, '#', 1]
# input_nums = [2, 1, 3, '#', 4]
input_nums = [1, '#', 4, '#', '#', 3, '#', '#', '#', '#', '#', '#', 2]
# input_nums = [4, 3, "#", '#', 2]
input_root = TreeNode.init_tree(input_nums)
print HouseRobberIII().rob(input_root)
