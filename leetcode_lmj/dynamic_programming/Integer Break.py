class IntegerBreak(object):
    def integerBreak(self, n):
        dp = [0 for x in range(0, n + 1)]
        dp[0] = 0
        dp[1] = dp[2] = 1
        if n <= 2:
            return dp[n]
        for i in range(3, n + 1):
            for j in range(1, i / 2 + 1):
                candidates = [dp[i], j * (i - j), dp[j] * dp[i - j],
                              dp[j] * (i - j), j * dp[i - j]]
                dp[i] = max(candidates)
        return dp[n]


print IntegerBreak().integerBreak(10)
