__author__ = 'LiMingji'

'''
��ʼ����sells[0] = 0��buys[0] = -prices[0]
sells[i] = max(buys[i - 1] + prices[i], sells[i - 1] + delta)
buys[i] = max(sells[i - 2] - prices[i], buys[i - 1] - delta)

https://leetcode.com/discuss/71354/share-my-thinking-process
'''


class BestTimeToBuyAndSellStockWithCooldown(object):
    def maxProfit(self, prices):
        size = len(prices)
        if not size:
            return 0
        buys = [None] * size
        sells = [None] * size
        sells[0], buys[0] = 0, -prices[0]
        for x in range(1, size):
            delta = prices[x] - prices[x - 1]
            sells[x] = max(buys[x - 1] + prices[x], sells[x - 1] + delta)
            buys[x] = max(buys[x - 1] - delta, sells[x - 2] - prices[x] if x > 1 else None)
        return max(sells)


input_list = [1, 2, 3, 0, 2]
# input_list = [3, 1]
s = BestTimeToBuyAndSellStockWithCooldown()
print s.maxProfit(input_list)

