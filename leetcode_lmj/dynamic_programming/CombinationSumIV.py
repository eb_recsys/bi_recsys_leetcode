class CombinationSumIV(object):
    def combinationSum4(self, nums, target):
        nums.sort()
        dp = [0 for x in range(0, target + 1)]

        for pos in range(1, target + 1):
            for num in nums:
                if num > pos:
                    break
                elif num == pos:
                    dp[pos] += 1
                else:
                    dp[pos] += dp[pos - num]
        return dp[target]


m = CombinationSumIV()
print m.combinationSum4(nums=[1, 2, 3], target=4)
print m.combinationSum4(nums=[1, 50], target=200)
