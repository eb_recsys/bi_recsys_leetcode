# encoding:UTF-8
__author__ = 'LiMingji'

'''
http://www.cnblogs.com/biyeymyhjob/archive/2012/09/28/2707343.html
if i == 0 且 j == 0，edit(i, j) = 0
if i == 0 且 j > 0，edit(i, j) = j
if i > 0 且j == 0，edit(i, j) = i
if i ≥ 1  且 j ≥ 1 ，edit(i, j) == min{ edit(i-1, j) + 1, edit(i, j-1) + 1, edit(i-1, j-1) + f(i, j) }，当第一个字符串的第i个字符不等于第二个字符串的第j个字符时，f(i, j) = 1；否则，f(i, j) = 0。


这样做是不对的，因为[0] * 5是一个一维数组的对象，* 3的话只是把对象的引用复制了3次，
'''


class EditDistance(object):
    def minDistance(self, word1, word2):
        word1 = '0' + word1
        word2 = '0' + word2
        # dp = [[0] * len(word2)] * len(word1)
        dp = [[0 for col in range(len(word2))] for row in range(len(word1))]
        i = 0
        while i < len(word1):
            j = 0
            while j < len(word2):
                if i == 0 and j == 0:
                    dp[i][j] = 0
                elif i == 0 and j != 0:
                    dp[i][j] = j
                elif j == 0 and i != 0:
                    dp[i][j] = i
                else:
                    if word1[i] == word2[j]:
                        dp[i][j] = min([dp[i - 1][j] + 1, dp[i][j - 1] + 1, dp[i - 1][j - 1]])
                    else:
                        dp[i][j] = min([dp[i - 1][j] + 1, dp[i][j - 1] + 1, dp[i - 1][j - 1] + 1])
                j += 1
            i += 1
        return dp[len(word1) - 1][len(word2) - 1]


str1 = "failing"
str2 = "sailn"
print EditDistance().minDistance(str2, str1)
