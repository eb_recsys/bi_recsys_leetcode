class DecodeWays(object):
    def numDecodings(self, s):
        if len(s) == 0 or s[0] == "0" or s.count("00") != 0:
            return 0
        if len(s) == 1:
            return 1
        dp = [1 for x in range(0, len(s))]

        dp[0] = 1

        if int(s[0:2]) in [10, 20]:
            dp[1] = 1
        elif int(s[0:2]) in range(11, 27):
            dp[1] = 2
        elif int(s[0:2]) < 10 or int(s[1]) == 0:
            dp[1] = 0
        else:
            dp[1] = 1

        for index in range(2, len(s)):
            cur = 0
            if int(s[index]) != 0:
                cur += dp[index - 1]
            if int(s[index - 1: index + 1]) in range(10, 27):
                cur += dp[index - 2]
            dp[index] = cur

        return dp[len(s) - 1]


m = DecodeWays()
print m.numDecodings("10")
