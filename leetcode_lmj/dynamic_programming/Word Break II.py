# encoding:UTF-8
__author__ = 'LiMingji'


class WordBreakII(object):
    def dfs(self, s, wordDict, start, cur, ret):
        if start >= len(s):
            retstr = ""
            for x in cur:
                retstr += x + " "
            ret.append(retstr[0:len(retstr) - 1])
            return
        index = start
        while index < len(s):
            if s[start:index + 1] in wordDict:
                cur.append(s[start:index + 1])
                self.dfs(s, wordDict, index + 1, cur, ret)
                cur.pop()
            index += 1

    def wordBreak(self, s, wordDict):
        cur = []
        ret = []
        self.dfs(s, wordDict, 0, cur, ret)
        return ret


input_str = "catsanddog"
dict = ["cat", "cats", "and", "sand", "dog"]

print WordBreakII().wordBreak(input_str, dict)
