class MaximumProductSubarray(object):
    def maxProduct(self, nums):
        dp_max = [0 for x in range(0, len(nums))]
        dp_min = [0 for x in range(0, len(nums))]

        dp_max[0] = nums[0]
        dp_min[0] = nums[0]

        for index in range(1, len(nums)):
            dp_max[index] = max([nums[index], nums[index] * dp_max[index - 1],
                                 nums[index] * dp_min[index - 1]])
            dp_min[index] = min(nums[index], nums[index] * dp_max[index - 1],
                                nums[index] * dp_min[index - 1])

        return max(dp_min + dp_max)


m = MaximumProductSubarray()
print m.maxProduct(nums=[2, 3, -2, 4])
