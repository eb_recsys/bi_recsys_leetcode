# encode=UTF-8

import re

'''
这题没太搞明白
'''


class DifferentWaysToAddParentheses(object):
    exprDict = dict()

    def dfs(self, nums, ops):
        if ops:
            for x in range(len(ops)):
                self.dfs(nums[:x] + ['(' + nums[x] + ops[x] + nums[x + 1] + ')'] + nums[x + 2:], ops[:x] + ops[x + 1:])
        elif nums[0] not in self.exprDict:
            self.exprDict[nums[0]] = eval(nums[0])

    def diffWaysToCompute(self, input):
        nums, ops = [], []
        input = re.sub(r'([\+\-*])', r' \1 ', input).split()
        for x in input:
            if x.isdigit():
                nums.append(x)
            else:
                ops.append(x)
        self.dfs(nums, ops)
        return self.exprDict.values()


print DifferentWaysToAddParentheses().diffWaysToCompute("2*3-4*5")
