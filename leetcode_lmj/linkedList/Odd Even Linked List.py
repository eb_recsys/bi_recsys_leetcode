# coding: UTF-8
__author__ = 'LiMingji'

'''
两个头，分别管理奇数链 和  偶数链
'''

import List


class OddEvenLinkedList(object):
    def oddEvenList(self, head):
        if head is None:
            return None
        dummy_odd = cur_odd = List.ListNode(-1)
        dummy_even = cur_even = List.ListNode(-1)

        count = 1
        cur = head
        while cur is not None:
            if count % 2 == 0:
                cur_even.next = cur
                cur = cur.next
                cur_even = cur_even.next
            else:
                cur_odd.next = cur
                cur = cur.next
                cur_odd = cur_odd.next
            count += 1

        cur_odd.next = dummy_even.next
        cur_even.next = None
        return dummy_odd.next


nums = [2, 1, 4, 3, 6, 5, 7, 8]
list_head = List.init_list(nums)
s = OddEvenLinkedList()
List.print_list(s.oddEvenList(list_head))


