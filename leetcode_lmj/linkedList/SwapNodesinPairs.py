# coding: UTF-8


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def swapPairs(self, head):
        if head is None or head.next is None:
            return head

        dummy = pre = ListNode(-1)
        dummy.next = head

        while pre.next is not None and pre.next.next is not None:
            nodeNo1 = pre.next
            nodeNo2 = pre.next.next
            pre.next = nodeNo2
            nodeNo1.next = nodeNo2.next
            nodeNo2.next = nodeNo1
            pre = nodeNo1
        return dummy.next



