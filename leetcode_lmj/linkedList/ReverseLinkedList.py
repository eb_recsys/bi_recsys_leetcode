# coding: UTF-8
__author__ = 'LiMingji'


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def reverseList(self, head):
        if head is None or head.next is None:
            return head
        dummy = ListNode(-1)
        current = head
        nextPtr = head.next
        while current is not None:
            current.next = dummy.next
            dummy.next = current
            current = nextPtr
            if nextPtr is None:
                nextPtr = None
            else:
                nextPtr = nextPtr.next
        return dummy.next

