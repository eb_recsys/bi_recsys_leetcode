import List


class ReverseLinkedListII(object):
    def reverse(self, head):
        dummy = List.ListNode(-1)
        while head is not None:
            cur = head
            head = head.next
            cur.next = dummy.next
            dummy.next = cur

        return dummy.next

    def reverseBetween(self, head, m, n):
        dummy = List.ListNode(-1)
        dummy.next = head

        cur = pre = dummy
        for index in range(m - 1):
            cur = cur.next
        pre = cur

        cur = tail_pre = tail = dummy
        for index in range(n):
            cur = cur.next
        tail_pre = cur
        tail = tail_pre.next
        tail_pre.next = None

        pre.next = self.reverse(pre.next)

        cur = pre.next
        while cur.next is not None:
            cur = cur.next
        cur.next = tail

        return dummy.next


m = ReverseLinkedListII()

num = [1, 2, 3, 4, 5]
h = List.init_list(num)
# h = m.reverseBetween(h, 2, 4)
h = m.reverseBetween(h, 1, 5)

List.print_list(h)
