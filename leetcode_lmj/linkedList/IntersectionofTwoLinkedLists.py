# coding: UTF-8
__author__ = 'LiMingji'


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):

    def getLen(self, head):
        count = 0
        while head is not None:
            count +=1
            head = head.next
        return count

    def getIntersectionNode(self, headA, headB):
        lenA = self.getLen(headA)
        lenB = self.getLen(headB)

        if lenA > lenB:
            n = lenA -lenB
            fast = headA
            slow = headB
        else:
            n = lenB - lenA
            fast = headB
            slow = headA

        for i in range(n):
            fast = fast.next

        while slow != fast:
            slow = slow.next
            fast = fast.next

        if slow is None:
            return None
        else:
            return slow
            


