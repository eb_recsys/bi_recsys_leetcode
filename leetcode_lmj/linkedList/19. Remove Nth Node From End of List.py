import List


class RemoveNthNodeFromEndOfList(object):
    def removeNthFromEnd(self, head, n):
        dummy = List.ListNode(-1)
        dummy.next = head
        fast = slow = dummy

        for index in range(n):
            fast = fast.next

        while fast.next is not None:
            fast = fast.next
            slow = slow.next

        slow.next = slow.next.next

        return dummy.next


m = RemoveNthNodeFromEndOfList()
num = [1]
h = List.init_list(num)
List.print_list(m.removeNthFromEnd(h, 1))
