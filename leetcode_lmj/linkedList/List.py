# coding: UTF-8
__author__ = 'LiMingji'


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


def print_list(head):
    while head is not None:
        print str(head.val) + "=>",
        head = head.next
    print


def init_list(num):
    dummy = cur = ListNode(-1)
    for x in num:
        cur.next = ListNode(x)
        cur = cur.next
    return dummy.next
