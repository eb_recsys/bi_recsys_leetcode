import List


class ReverseNodesInKGroup(object):
    def reverse(self, head):
        dummy = List.ListNode(-1)
        tail = head
        while head is not None:
            cur = head
            head = head.next
            cur.next = dummy.next
            dummy.next = cur

        return [dummy.next, tail]

    def reverseKGroup(self, head, k):
        dummy = List.ListNode(-1)
        dummy.next = head

        pre = dummy
        while head is not None:
            index = 1
            while index < k and head is not None:
                head = head.next
                index += 1
            if head is None:
                break
            next_phase = head.next
            head.next = None

            new_points = self.reverse(pre.next)
            pre.next = new_points[0]
            new_points[1].next = next_phase

            head = next_phase
            pre = new_points[1]

        return dummy.next


m = ReverseNodesInKGroup()
num = [1, 2, 3, 4, 5]
h = List.init_list(num)
List.print_list(m.reverseKGroup(h, 3))
