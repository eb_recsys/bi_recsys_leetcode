import List


class RotateList(object):
    def rotateRight(self, head, k):
        if head is None:
            return
        length = 1
        cur = head
        while cur.next is not None:
            length += 1
            cur = cur.next
        cur.next = head

        k = k % length
        cur = head
        for x in range(1, length - k):
            cur = cur.next

        new_head = cur.next
        cur.next = None
        return new_head


m = RotateList()
nums = [1, 2, 3, 4, 5]
nums = [1, 2]
h = List.init_list(nums)
List.print_list(h)
List.print_list(m.rotateRight(h, 2))
