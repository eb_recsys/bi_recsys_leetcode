# coding: UTF-8
__author__ = 'LiMingji'


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def partition(self, head, x):
        smaller = smallerCur = ListNode(-1)
        bigger  = biggerCur = ListNode(-1)

        cur = head
        while cur is not None:
            if cur.val < x:
                smallerCur.next =cur
                smallerCur = smallerCur.next
            else:
                biggerCur.next = cur
                biggerCur = biggerCur.next
            cur = cur.next
        biggerCur.next = None
        smallerCur.next = bigger.next
        return smaller.next




