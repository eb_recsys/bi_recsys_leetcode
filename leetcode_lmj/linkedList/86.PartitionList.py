import List


class PartitionList(object):
    def partition(self, head, x):
        left_cur = dummy_left = List.ListNode(-1)
        right_cur = dummy_right = List.ListNode(-1)

        while head is not None:
            cur = head
            head = head.next
            if cur.val < x:
                left_cur.next = cur
                left_cur = left_cur.next
            else:
                right_cur.next = cur
                right_cur = right_cur.next

        right_cur.next = None
        left_cur.next = dummy_right.next
        return dummy_left.next


m = PartitionList()
num = [1, 4, 3, 2, 5, 2]

h = List.init_list(num)
List.print_list(m.partition(h, 3))
