__author__ = 'LiMingji'


class FindKthElem(object):
    def find_kth_elem(self, nums, start, end):
        left = start
        right = end
        target = nums[start]
        while left < right:
            while left < right and nums[right] >= target:
                right -= 1
            nums[left] = nums[right]
            while left < right and nums[left] <= target:
                left += 1
            nums[right] = nums[left]
        nums[left] = target
        return left

    def find_kth_recursion(self, nums, k, low, high):
        if low >= high:
            return nums[low]
        else:
            mid = self.find_kth_elem(nums, low, high)
            if mid == k:
                return nums[mid]
            elif mid > k:
                return self.find_kth_recursion(nums, k, low, mid-1)
            elif mid < k:
                return self.find_kth_recursion(nums, k, mid+1, high)

    def find_kth(self, nums, k):
        return self.find_kth_recursion(nums, k-1, 0, len(nums)-1)

input = [3, 1, 2, 4, 5, 3, 2, 1, 4, 5, 6]
s = FindKthElem()
for x in range(1, len(input)+1):
    print s.find_kth(input, x)



