import List


class SwapNodesInPairs(object):
    def swapPairs(self, head):
        if head is None:
            return head

        dummy = List.ListNode(-1)
        dummy.next = head

        pre = dummy

        while pre.next is not None and pre.next.next is not None:
            first = pre.next
            second = pre.next.next
            tail = pre.next.next.next

            pre.next = second
            second.next = first
            first.next = tail

            pre = first

        return dummy.next


m = SwapNodesInPairs()
num = [1, 2, 3, 4]
h = List.init_list(num)
List.print_list(m.swapPairs(h))
