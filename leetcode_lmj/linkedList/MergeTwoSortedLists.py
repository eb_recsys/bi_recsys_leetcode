# coding: UTF-8
__author__ = 'LiMingji'


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def mergeTwoLists(self, l1, l2):
        dummy = current = ListNode(-1)
        while l1 is not None and l2 is not None :
            if l1.val < l2.val:
                current.next = l1
                l1 = l1.next
            else:
                current.next = l2
                l2 = l2.next
            current = current.next

        if l1 is not None :
            current.next = l1
        else:
            current.next = l2
        return dummy.next


