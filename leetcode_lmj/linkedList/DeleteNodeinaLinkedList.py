# coding: UTF-8
__author__ = 'LiMingji'


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def deleteNode(self, node):
        if node.next is None:
            node = None
        else:
            node.val = node.next.val
            node.next = node.next.next
