import List


class ReorderList(object):
    def reverse(self, head):
        dummy = List.ListNode(-1)
        while head is not None:
            cur = head
            head = head.next
            cur.next = dummy.next
            dummy.next = cur
        return dummy.next

    def reorderList(self, head):
        slow = fast = head

        while fast is not None and fast.next is not None:
            fast = fast.next.next
            slow = slow.next

        new_head = self.reverse(slow)
        cur = head
        cur_new = new_head

        while cur is not None and cur_new is not None:
            head = head.next
            cur.next = cur_new
            cur = cur.next
            cur_new = cur_new.next
            cur.next = head
            cur = cur.next
        if cur is not None:
            cur.next = None


m = ReorderList()
nums = [1, 2, 3]
h = List.init_list(nums)
m.reorderList(h)
List.print_list(h)
