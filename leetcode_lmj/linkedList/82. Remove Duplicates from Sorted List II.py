import List


class RemoveDuplicatesFromSortedListII(object):
    def deleteDuplicates(self, head):
        if head is None:
            return head

        dummy = List.ListNode(-1)
        dummy.next = head
        pre = dummy

        while pre.next is not None:
            next_cur = pre.next.next
            if next_cur is None:
                break
            if next_cur is not None and pre.next.val == next_cur.val:
                while next_cur is not None and pre.next.val == next_cur.val:
                    next_cur = next_cur.next
                pre.next = next_cur
            else:
                pre = pre.next
        return dummy.next


m = RemoveDuplicatesFromSortedListII()
num = [1, 2, 3, 3, 4, 4, 5]
h = List.init_list(num)

List.print_list(m.deleteDuplicates(h))
