# coding: UTF-8
__author__ = 'LiMingji'


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def insertionSortList(self, head):
        if head is None or head.next is None:
            return head

        dummy = ListNode(-1)
        next = head

        while next is not None:
            cur = next
            next = next.next

            dummypre = dummy
            dummycur = dummy.next
            while dummycur is not None:
                if cur.val <= dummycur.val:
                    break;
                dummycur = dummycur.next
                dummypre = dummypre.next
            if dummycur is None:
                cur.next = None
                dummypre.next = cur
            else:
                cur.next = dummycur
                dummypre.next = cur

        return dummy.next



