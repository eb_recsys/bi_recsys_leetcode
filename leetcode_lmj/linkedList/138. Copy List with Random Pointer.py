class RandomListNode(object):
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None


class CopyListWithRandomPointer(object):
    def copyRandomList(self, head):
        dummy = RandomListNode(-1)
        dummy.next = head

        cur = head
        while cur is not None:
            new_node = RandomListNode(cur.label)
            new_node.next = cur.next
            cur.next = new_node
            cur = cur.next.next

        cur = dummy.next
        while cur is not None:
            if cur.random is not None:
                cur.next.random = cur.random.next
            cur = cur.next.next

        dummy_new = RandomListNode(-1)
        cur_new = dummy

        cur = dummy.next
        while cur is not None:
            cur_new.next = cur.next
            cur_new = cur_new.next
            cur.next = cur.next.next
            cur = cur.next

        return dummy_new.next
