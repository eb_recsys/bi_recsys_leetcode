import List


class AddTwoNumbers(object):
    def addTwoNumbers(self, l1, l2):
        binary = 0
        dummy = List.ListNode(-1)
        cur_node = dummy

        while l1 is not None or l2 is not None:
            cur1 = l1.val if l1 is not None else 0
            cur2 = l2.val if l2 is not None else 0
            new_node = List.ListNode((cur1 + cur2 + binary) % 10)
            binary = (cur1 + cur2 + binary) / 10

            cur_node.next = new_node
            cur_node = cur_node.next

            l1 = l1.next if l1 is not None else None
            l2 = l2.next if l2 is not None else None

        if binary == 1:
            new_node = List.ListNode(1)
            cur_node.next = new_node

        return dummy.next


num1 = [1]
num2 = [9, 9]
head1 = List.init_list(num1)
head2 = List.init_list(num2)

List.print_list(head1)
List.print_list(head2)

i = AddTwoNumbers()

head = i.addTwoNumbers(head1, head2)
List.print_list(head)
