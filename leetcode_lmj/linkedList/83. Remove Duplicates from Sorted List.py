import List


class RemoveDuplicatesFromSortedList(object):
    def deleteDuplicates(self, head):
        dummy = List.ListNode(-1000)
        dummy.next = head
        pre = dummy
        cur = head

        while cur is not None:
            if pre.val == cur.val:
                pre.next = cur.next
                cur = pre.next
            else:
                cur = cur.next
                pre = pre.next

        return dummy.next


num = [1, 1, 2, 3, 3]
num = [1, 1, 2]
h = List.init_list(num)

m = RemoveDuplicatesFromSortedList()
List.print_list(m.deleteDuplicates(h))
