

SELECT employees.name
FROM Employee AS employees LEFT JOIN Employee AS managers ON employees.managerid = managers.id
WHERE employees.salary > managers.salary;