class TreeLinkNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        self.next = None


class PopulatingNextRightPointersInEachNode(object):
    def connect(self, root):
        if root is None:
            return
        head = root
        while head is not None:
            cur = head
            while cur is not None and cur.left is not None:
                cur.left.next = cur.right
                if cur.next is not None:
                    cur.right.next = cur.next.left
                cur = cur.next
            head = head.left
