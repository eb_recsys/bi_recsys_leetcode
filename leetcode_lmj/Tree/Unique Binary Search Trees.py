class UniqueBinarySearchTrees(object):
    def numTrees(self, n):
        dp = [0 for m in range(max(n + 1, 4))]
        dp[0] = 1
        dp[1] = 1
        dp[2] = 2
        dp[3] = 5
        if n < 4:
            return dp[n]
        for i in range(4, n + 1):
            for j in range(0, i):
                dp[i] += dp[j] * dp[i - j - 1]
        return dp[n]


print UniqueBinarySearchTrees().numTrees(4)
