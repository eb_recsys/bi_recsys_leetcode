import TreeNode


class FlattenBinaryTreeToLinkedList(object):
    def transform(self, node):
        if node.left is None and node.right is None:
            return node
        if node.left is None and node.right is not None:
            return self.transform(node.right)
        if node.right is None and node.left is not None:
            left_return = self.transform(node.left)
            node.right = node.left
            node.left = None
            return left_return
        if node.left is not None and node.right is not None:
            left_return = self.transform(node.left)
            right_return = self.transform(node.right)
            left_return.right = node.right
            node.right = node.left
            node.left = None
            return right_return

    def flatten(self, root):
        if root is None or (root.left is None and root.right is None):
            return
        self.transform(root)


input_nums = [1, 2]
input_root = TreeNode.init_tree(input_nums)
FlattenBinaryTreeToLinkedList().flatten(input_root)
TreeNode.pre_order_traversal(input_root)
print
TreeNode.in_order_traversal(input_root)
