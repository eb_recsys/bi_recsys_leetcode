import TreeNode


class BinaryTreeLevelOrderTraversalII(object):
    def levelOrderBottom(self, root):
        ret = []
        if root is None:
            return ret
        next_level = [root]
        while len(next_level) != 0:
            cur_level = next_level[:]
            next_level = []
            cur_ret = []
            for cur_node in cur_level:
                if cur_node is None:
                    continue
                else:
                    cur_ret.append(cur_node.val)
                    next_level.append(cur_node.left)
                    next_level.append(cur_node.right)
            if len(cur_ret) > 0:
                ret.append(cur_ret)
        ret.reverse()
        return ret


input_nums = [1, 2, 3, 4, 5]
input_root = TreeNode.init_tree(input_nums)

print BinaryTreeLevelOrderTraversalII().levelOrderBottom(input_root)
