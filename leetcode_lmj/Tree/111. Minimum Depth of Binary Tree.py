class MinimumDepthOfBinaryTree(object):
    def minDepth(self, root):
        if root is None:
            return 0
        next_level = [root]

        height = 1
        while len(next_level) != 0:
            cur_level = next_level[:]
            next_level = []
            for node in cur_level:
                if node.left is None and node.right is None:
                    return height
                if node.left is not None:
                    next_level.append(node.left)
                if node.right is not None:
                    next_level.append(node.right)
            height += 1
        return height
