import TreeNode


class BinaryTreeLevelOrderTraversalII(object):
    def levelOrderBottom(self, root):
        ret = []
        if root is None:
            return ret
        next_level = [root]
        while len(next_level) != 0:
            cur_level = next_level[:]
            next_level = []
            cur_ret = []
            for cur_node in cur_level:
                if cur_node is None:
                    continue
                else:
                    cur_ret.append(cur_node.val)
                    next_level.append(cur_node.left)
                    next_level.append(cur_node.right)
            if len(cur_ret) > 0:
                ret.append(cur_ret)
        ret.reverse()
        return ret


nums = [3, 9, 20, '#', '#', 15, 7]
root = TreeNode.init_tree(nums)
m = BinaryTreeLevelOrderTraversalII()
print m.levelOrderBottom(root)
