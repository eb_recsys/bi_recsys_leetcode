import TreeNode


class BalancedBinaryTree(object):
    def balanced_height(self, root):
        if root is None:
            return 0
        left = self.balanced_height(root.left)
        right = self.balanced_height(root.right)
        if left < 0 or right < 0 or abs(left - right) > 1:
            return -1
        return max(left, right) + 1

    def isBalanced(self, root):
        if self.balanced_height(root) >= 0:
            return True
        return False


input_nums = [1, 2, "#", 3]
input_root = TreeNode.init_tree(input_nums)
print BalancedBinaryTree().isBalanced(input_root)
