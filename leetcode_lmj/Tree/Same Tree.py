class SameTree(object):
    def isSameTree(self, p, q):
        if p == q is None:
            return True
        elif p is None or q is None:
            return False
        else:
            return p.val == q.val and self.isSameTree(p.left, q.left) \
                   and self.isSameTree(p.right, q.right)
