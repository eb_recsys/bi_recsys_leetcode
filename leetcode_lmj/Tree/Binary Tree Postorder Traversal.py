import TreeNode


class BinaryTreePostorderTraversal(object):
    def postorderTraversal(self, root):
        ret = []
        if root is None:
            return ret
        stack = []
        sets = set()
        stack.append(root)
        while len(stack) != 0:
            cur_node = stack.pop()
            if cur_node is None:
                continue
            if cur_node in sets:
                ret.append(cur_node.val)
            else:
                sets.add(cur_node)
                stack.append(cur_node)
                stack.append(cur_node.right)
                stack.append(cur_node.left)
        return ret


input_nums = [1, 2, 3, 4, 5]
input_root = TreeNode.init_tree(input_nums)
TreeNode.post_order_traversal(input_root)
print

print BinaryTreePostorderTraversal().postorderTraversal(input_root)
