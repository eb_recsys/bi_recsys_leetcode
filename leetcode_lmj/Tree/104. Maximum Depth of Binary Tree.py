class MaximumDepthOfBinaryTree(object):
    def maxDepth(self, root):
        if root is None:
            return 0
        next_level = [root]
        height = 0

        while len(next_level) != 0:
            height += 1
            cur_level = next_level[:]
            next_level = []
            for node in cur_level:
                if node.left is not None:
                    next_level.append(node.left)
                if node.right is not None:
                    next_level.append(node.right)
        return height
