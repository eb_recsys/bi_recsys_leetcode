class LowestCommonAncestorOfABinarySearchTree(object):
    def lowestCommonAncestor(self, root, p, q):
        if min(p.val, q.val) <= root.val <= max(p.val, q.val):
            return root
        if root.val > max(p.val, q.val):
            return self.lowestCommonAncestor(root.left, p, q)
        if root.val < min(p.val, q.val):
            return self.lowestCommonAncestor(root.right, p, q)
