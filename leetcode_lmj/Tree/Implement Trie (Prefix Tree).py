__author__ = 'LiMingji'


class TrieNode(object):
    def __init__(self):
        self.val = 'root'
        self.children = [None] * 26
        self.isEndNode = False


class Trie(object):
    def __init__(self):
        self.root = TrieNode()

    def insert(self, word):
        cur_node = self.root
        for x in word:
            if cur_node.children[ord(x)-ord('a')] is None:
                new_node = TrieNode()
                new_node.val = x
                new_node.isEndNode = False
                cur_node.children[ord(x)-ord('a')] = new_node
            cur_node = cur_node.children[ord(x)-ord('a')]
        cur_node.isEndNode = True

    def search(self, word):
        cur_node = self.root
        for x in word:
            if cur_node.children[ord(x)-ord('a')] is None:
                return False
            else:
                cur_node = cur_node.children[ord(x)-ord('a')]
        return cur_node.isEndNode

    def startsWith(self, prefix):
        cur_node = self.root
        for x in prefix:
            if cur_node.children[ord(x)-ord('a')] is None:
                return False
            else:
                cur_node = cur_node.children[ord(x)-ord('a')]
        return True

# Your Trie object will be instantiated and called as such:
trie = Trie()
trie.insert("app")
trie.insert("apple")
trie.insert("beer")
trie.insert("add")
trie.insert("jam")
trie.insert("rental")
print trie.search("apps")
print trie.search("app")
print trie.search("ad")
print trie.search("applepie")
print trie.search("rest")
print trie.search("jan")
print trie.search("rent")
print trie.search("beer")
print trie.search("jam")
print trie.startsWith("apps")
print trie.startsWith("app")
print trie.startsWith("ad")
print trie.startsWith("applepie")
print trie.startsWith("rest")
print trie.startsWith("jan")
print trie.startsWith("rent")
print trie.startsWith("beer")
print trie.startsWith("jam")

