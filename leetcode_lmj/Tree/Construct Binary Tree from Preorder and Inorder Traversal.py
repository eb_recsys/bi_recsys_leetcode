import TreeNode


class ConstructBinaryTreeFromPreorderAndInorderTraversal(object):
    def build(self, nums, start, end, dicts):
        if start > end:
            return None
        elif start == end:
            return TreeNode.TreeNode(nums[start])
        else:
            min_pos = start
            for i in range(start + 1, end + 1):
                if dicts[nums[i]] < dicts[nums[start]]:
                    min_pos = i
            cur = TreeNode.TreeNode(nums[min_pos])
            cur.left = self.build(nums, start, min_pos - 1, dicts)
            cur.right = self.build(nums, min_pos + 1, end, dicts)
            return cur

    def buildTree(self, preorder, inorder):
        dicts = {}
        for i in range(0, len(preorder)):
            dicts[preorder[i]] = i
        return self.build(inorder, 0, len(inorder) - 1, dicts)


preorder_input = [1]
inorder_input = [1]

node = ConstructBinaryTreeFromPreorderAndInorderTraversal().buildTree(preorder_input, inorder_input)
TreeNode.pre_order_traversal(node)
print
TreeNode.in_order_traversal(node)
