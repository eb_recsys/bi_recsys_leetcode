import TreeNode


class UniqueBinarySearchTreesII(object):
    def generate_node(self, nums, start, end):
        if start > end:
            return [None]
        elif start == end:
            return [TreeNode.TreeNode(nums[start])]
        ret = []
        for index in range(start, end + 1):
            lefts = self.generate_node(nums, start, index - 1)
            rights = self.generate_node(nums, index + 1, end)
            for left in lefts:
                for right in rights:
                    cur_node = TreeNode.TreeNode(nums[index])
                    cur_node.left = left
                    cur_node.right = right
                    ret.append(cur_node)
            index += 1
        return ret

    def generateTrees(self, n):
        ret = []
        if n == 0:
            return ret
        nums = []
        for i in range(1, n + 1):
            nums.append(i)
        return self.generate_node(nums, 0, len(nums) - 1)


for node in UniqueBinarySearchTreesII().generateTrees(3):
    TreeNode.pre_order_traversal(node)
    print


