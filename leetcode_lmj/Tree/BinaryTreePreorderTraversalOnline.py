import TreeNode

__author__ = 'LiMingji'


class BinaryTreePreorderTraversal(object):
    @staticmethod
    def pre_order_traversal(root):
        ret = []
        stack = []
        while root is not None or len(stack) > 0:
            while root is not None:
                ret.append(root.val)
                stack.append(root)
                root = root.left
            if len(stack) > 0:
                root = stack.pop()
                root = root.right
        return ret

    @staticmethod
    def in_order_traversal(root):
        ret = []
        stack = []
        while root is not None or len(stack) > 0:
            while root is not None:
                stack.append(root)
                root = root.left
            if len(stack) > 0:
                root = stack.pop()
                ret.append(root.val)
                root = root.right
        return ret


input_nums = [1, 2, 3, 4, 5]
input_root = TreeNode.init_tree(input_nums)
TreeNode.pre_order_traversal(input_root)
print

print BinaryTreePreorderTraversal().postorder_traversal(input_root)
