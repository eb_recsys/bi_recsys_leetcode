import TreeNode


class RecoverBinarySearchTree(object):
    def in_order(self, root, in_order):
        if root is not None:
            self.in_order(root.left, in_order)
            in_order.append(root)
            self.in_order(root.right, in_order)

    def recoverTree(self, root):
        in_order = []
        self.in_order(root, in_order)
        left = 0
        while left < len(in_order) - 1:
            if in_order[left].val > in_order[left + 1].val:
                break
            left += 1

        right = len(in_order) - 1
        while right > 0:
            if in_order[right].val < in_order[right - 1].val:
                break
            right -= 1
        in_order[left].val, in_order[right].val = in_order[right].val, in_order[left].val


input_nums = [3, 4, 5, 1, '#', 2]
input_root = TreeNode.init_tree(input_nums)
RecoverBinarySearchTree().recoverTree(input_root)
TreeNode.pre_order_traversal(input_root)
print
TreeNode.in_order_traversal(input_root)
