import TreeNode


class BinaryTreePaths(object):
    def dfs(self, root, cur, ret):
        if root is None:
            return

        if root.left is None and root.right is None:
            tmp = ""
            for index in range(len(cur) - 1):
                tmp += str(cur[index]) + "->"
            tmp += str(cur[len(cur) - 1])
            ret.append(tmp)
            return

        if root.left is not None:
            cur.append(root.left.val)
            self.dfs(root.left, cur, ret)
            cur.pop(len(cur) - 1)
        if root.right is not None:
            cur.append(root.right.val)
            self.dfs(root.right, cur, ret)
            cur.pop(len(cur) - 1)

    def binaryTreePaths(self, root):
        ret = []
        if root is None:
            return ret
        cur = [root.val]
        self.dfs(root, cur, ret)
        return ret


nums = [3, 9, 20, '#', '#', 15, 7]
nums = [1, 2, 3, '#', '5']
root = TreeNode.init_tree(nums)
m = BinaryTreePaths()
print m.binaryTreePaths(root)
