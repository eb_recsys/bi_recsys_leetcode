import TreeNode
import sys


class ValidateBinarySearchTree(object):
    def validate(self, root, lower, upper):
        if root is None:
            return True
        if lower < root.val < upper:
            return self.validate(root.left, lower, root.val) and self.validate(root.right, root.val, upper)
        return False

    def isValidBST(self, root):
        return self.validate(root, -sys.maxint, sys.maxint)


input_nums = [10, 5, 15, 'null', 'null', 6, 20]
input_root = TreeNode.init_tree(input_nums)
print ValidateBinarySearchTree().isValidBST(input_root)
