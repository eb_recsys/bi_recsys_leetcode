import TreeNode


class SymmetricTree(object):
    def compare(self, left, right):
        if left is None and right is None:
            return True
        elif left is None or right is None:
            return False
        elif left.val == right.val:
            return True
        else:
            return False

    def isSymmetric(self, root):
        next_level = [root]
        while len(next_level) > 0:
            cur_level = next_level[:]
            next_level = []
            for cur_node in cur_level:
                if cur_node is None:
                    continue
                next_level.append(cur_node.left)
                next_level.append(cur_node.right)
            if len(next_level) != 0 and len(next_level) % 2 != 0:
                return False
            left = 0
            right = len(next_level) - 1
            while left < right:
                if self.compare(next_level[left], next_level[right]) is False:
                    return False
                left += 1
                right -= 1
        return True


input_nums = [1, 2, 2, 3, 3]
input_root = TreeNode.init_tree(input_nums)
print SymmetricTree().isSymmetric(input_root)
