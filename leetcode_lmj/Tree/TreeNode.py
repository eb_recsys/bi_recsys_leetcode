# Definition for a binary tree node.


class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

    def __repr__(self):
        return str(self.val)
        # return str(self.val) + " " + str(self.left) + str(self.right)


def init_node(nums, pos):
    if pos < len(nums) and nums[pos] != '#' and nums[pos] != "null":
        cur_node = TreeNode(nums[pos])
        cur_node.left = init_node(nums, pos * 2 + 1)
        cur_node.right = init_node(nums, pos * 2 + 2)
        return cur_node
    else:
        return None


def init_tree(nums):
    return init_node(nums, 0)


def pre_order_traversal(root):
    if root is None:
        return
    print str(root.val) + "=>",
    pre_order_traversal(root.left)
    pre_order_traversal(root.right)


def in_order_traversal(root):
    if root is None:
        return
    in_order_traversal(root.left)
    print str(root.val) + "=>",
    in_order_traversal(root.right)


def post_order_traversal(root):
    if root is None:
        return
    post_order_traversal(root.left)
    post_order_traversal(root.right)
    print str(root.val) + "=>",


def level_order(root):
    ret = []
    if root is None:
        return ret
    next_level = [root]
    while len(next_level) != 0:
        cur_level = next_level[:]
        next_level = []
        cur_ret = []
        for cur_node in cur_level:
            if cur_node is None:
                continue
            else:
                print str(cur_node.val) + "=>",
                next_level.append(cur_node.left)
                next_level.append(cur_node.right)
        if len(cur_ret) > 0:
            ret.append(cur_ret)
