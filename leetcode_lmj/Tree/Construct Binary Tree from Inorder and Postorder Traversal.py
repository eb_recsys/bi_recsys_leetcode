import TreeNode


class ConstructBinaryTreeFromInorderAndPostorderTraversal(object):
    def build(self, nums, start, end, dicts):
        if start > end:
            return None
        elif start == end:
            return TreeNode.TreeNode(nums[start])
        else:
            max_pos = start
            for i in range(start + 1, end + 1):
                if dicts[nums[i]] > dicts[nums[max_pos]]:
                    max_pos = i
            cur = TreeNode.TreeNode(nums[max_pos])
            cur.left = self.build(nums, start, max_pos - 1, dicts)
            cur.right = self.build(nums, max_pos + 1, end, dicts)
            return cur

    def buildTree(self, inorder, postorder):
        dicts = {}
        for i in range(0, len(postorder)):
            dicts[postorder[i]] = i
        return self.build(inorder, 0, len(inorder) - 1, dicts)


inorder_input = [4, 2, 5, 1, 3]
psotorder_input = [4, 5, 2, 3, 1]

node = ConstructBinaryTreeFromInorderAndPostorderTraversal().buildTree(inorder_input, psotorder_input)
TreeNode.pre_order_traversal(node)
print
TreeNode.in_order_traversal(node)
print
TreeNode.post_order_traversal(node)
