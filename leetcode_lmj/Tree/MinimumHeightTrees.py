class MinimumHeightTrees(object):
    def findMinHeightTrees(self, n, edges):
        # if len(edges) == 0:
        #     return [0]
        #
        # nodes_map = {}
        # for x, y in edges:
        #     if x in nodes_map:
        #         nodes_map[x].append(y)
        #     else:
        #         cur = [y]
        #         nodes_map[x] = cur
        #
        #     if y in nodes_map:
        #         nodes_map[y].append(x)
        #     else:
        #         cur = [x]
        #         nodes_map[y] = cur
        #
        # while len(nodes_map) > 2:
        #     leaves = []
        #     for key in nodes_map:
        #         if len(nodes_map[key]) == 1:
        #             leaves.append(key)
        #
        #     for key in leaves:
        #         another_node = nodes_map[key][0]
        #         nodes_map.pop(key)
        #         nodes_map[another_node].remove(key)
        # return nodes_map.keys()

        if n == 1: return [0]
        adj = [set() for _ in xrange(n)]
        for i, j in edges:
            adj[i].add(j)
            adj[j].add(i)

        leaves = [i for i in xrange(n) if len(adj[i]) == 1]

        while n > 2:
            n -= len(leaves)
            newLeaves = []
            for i in leaves:
                j = adj[i].pop()
                adj[j].remove(i)
                if len(adj[j]) == 1: newLeaves.append(j)
            leaves = newLeaves
        return leaves

m = MinimumHeightTrees()
n = 6
edges = [[0, 3], [1, 3], [2, 3], [4, 3], [5, 4]]
# n = 1
# edges = []

print m.findMinHeightTrees(n, edges)
