import TreeNode


class InvertBinaryTree(object):
    def do(self, root):
        if root is None or (root.left is None and root.right is None):
            return
        elif root.left is None:
            root.left = root.right
            root.right = None
            self.do(root.left)
            return
        elif root.right is None:
            root.right = root.left
            root.left = None
            self.do(root.right)
            return
        else:
            root.left, root.right = root.right, root.left
            self.do(root.left)
            self.do(root.right)

    def invertTree(self, root):
        self.do(root)
        return root


m = InvertBinaryTree()
nums = [4, 2, 7, 1, 3, 6, 9]
root = TreeNode.init_tree(nums)
TreeNode.level_order(m.invertTree(root))
