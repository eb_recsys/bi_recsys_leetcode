import TreeNode


class SymmetricTree(object):
    def isSymmetric2(self, left, right):
        if left is None and right is None:
            return True
        if left is None or right is None:
            return True
        return left.val == right.val and \
               self.isSymmetric2(left.left, right.right) and \
               self.isSymmetric2(left.right, right.left)

    def isSymmetric(self, root):
        if root is None:
            return True
        return self.isSymmetric2(root.left, root.right)


m = SymmetricTree()
nums = [1, 2, 2, 3, 4, 4, 3]
root = TreeNode.init_tree(nums)

print m.isSymmetric(root)
