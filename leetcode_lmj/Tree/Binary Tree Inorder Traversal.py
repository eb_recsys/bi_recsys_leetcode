import TreeNode

__author__ = 'LiMingji'


class BinaryTreeInorderTraversal(object):
    def inorderTraversal(self, root):
        ret = []
        stack = []
        if root is None:
            return ret
        sets = set()
        stack.append(root)
        while len(stack) != 0:
            cur_node = stack.pop()
            if cur_node is None:
                continue
            if cur_node in sets:
                ret.append(cur_node.val)
            else:
                sets.add(cur_node)
                stack.append(cur_node.right)
                stack.append(cur_node)
                stack.append(cur_node.left)
        return ret


input_nums = [1, 2, 3, 4, 5]
input_root = TreeNode.init_tree(input_nums)
TreeNode.in_order_traversal(input_root)
print

print BinaryTreeInorderTraversal().inorderTraversal(input_root)
