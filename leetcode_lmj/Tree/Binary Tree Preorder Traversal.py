import TreeNode

__author__ = 'LiMingji'


class BinaryTreePreorderTraversal(object):
    def preorderTraversal(self, root):
        ret = []
        stack = []
        if root is None:
            return ret
        stack.append(root)
        while len(stack) != 0:
            cur_node = stack.pop()
            if cur_node is None:
                continue
            ret.append(cur_node.val)
            stack.append(cur_node.right)
            stack.append(cur_node.left)
        return ret

input_nums = [1, 2, 3, 4, 5]
input_root = TreeNode.init_tree(input_nums)
TreeNode.pre_order_traversal(input_root)
print

print BinaryTreePreorderTraversal().preorderTraversal(input_root)



