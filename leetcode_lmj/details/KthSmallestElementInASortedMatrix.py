class KthSmallestElementInASortedMatrix(object):
    def kthSmallest(self, matrix, k):
        all_nums = []
        for line in matrix:
            for x in line:
                all_nums.append(x)
        all_nums.sort()
        return all_nums[k - 1]


matrix = [
    [1, 5, 9],
    [10, 11, 13],
    [12, 13, 15]
]
k = 8

m = KthSmallestElementInASortedMatrix()
print m.kthSmallest(matrix, k)
