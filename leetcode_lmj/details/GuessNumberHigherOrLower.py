def guess(num):
    val = 10
    if num == val:
        return 0
    elif num > val:
        return -1
    elif num < val:
        return 1


class GuessNumberHigherOrLower(object):
    def guessNumber(self, n):
        low = 1
        high = n

        while low <= high:
            mid = low + (high - low) / 2
            ret = guess(mid)
            if ret == 0:
                return mid
            elif ret == -1:
                high = mid - 1
            else:
                low = mid + 1


m = GuessNumberHigherOrLower()
print m.guessNumber(10)
