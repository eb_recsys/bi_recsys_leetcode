import math

'''
确定其是2的次方数了之后，发现只要是4的次方数，减1之后可以被3整除，所以可以写出代码如下：
'''


class PowerOfFour(object):
    def isPowerOfFour(self, num):
        # return 0 < num == 4 ** round(math.log(num, 4))
        return 0 < num and (num & (num - 1)) == (num - 1) % 3 == 0


print PowerOfFour().isPowerOfFour(7)
