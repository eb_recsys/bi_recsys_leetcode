class LargestNumber(object):
    def compare(selef, x, y):
        x_before_y = int(str(x) + str(y))
        y_before_x = int(str(y) + str(x))

        if x_before_y == y_before_x:
            return 0
        elif x_before_y > y_before_x:
            return 1
        elif x_before_y < y_before_x:
            return -1

    def largestNumber(self, nums):
        nums.sort(cmp=self.compare, reverse=True)
        ret = ""
        for x in range(0, len(nums)):
            ret += str(nums[x])
        return str(int(ret))


m = LargestNumber()
# print m.largestNumber(nums=[1, 2, 3, 4, 5])
# print m.largestNumber(nums=[3, 30, 34, 5, 9])
# print m.largestNumber(nums=[2, 1])
# print m.largestNumber(nums=[0, 0])
print m.largestNumber(nums=[824, 938, 1399, 5607, 6973, 5703, 9609, 4398, 8247])
# print m.compare(121, 12)
# print m.largestNumber(nums=[121, 12])
