import math

__author__ = 'LiMingji'


class PowerOfThree(object):
    def isPowerOfThree(self, n):
        return 3 ** round(math.log(n, 3)) == n > 0


s = PowerOfThree()
print s.isPowerOfThree(9)
