import collections


class TopKFrequentElements(object):
    def topKFrequent(self, nums, k):
        c = collections.Counter(nums)
        return [x[0] for x in c.most_common(k)]


m = TopKFrequentElements()
print m.topKFrequent2(nums=[1, 1, 1, 1, 2, 2, 3], k=2)
