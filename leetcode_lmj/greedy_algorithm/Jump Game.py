__author__ = 'LiMingji'


class JumpGame(object):
    def canJump(self, nums):
        reach = 1
        index = 0
        while index < reach < len(nums):
            reach = max(reach, index + 1 + nums[index])
            index += 1
        return reach >= len(nums)

# input_nums = [2, 3, 1, 1, 4]
# input_nums = [3, 2, 1, 0, 4]
# input_nums = [2, 1]
input_nums = [0, 1]

s = JumpGame()
print s.canJump(input_nums)
