__author__ = 'LiMingji'


class BestTimeToBuyAndSellStockII(object):
    def maxProfit(self, prices):
        if len(prices) is 0:
            return 0
        index = 1
        max_profit = 0
        while index < len(prices):
            if prices[index] > prices[index-1]:
                max_profit += prices[index] - prices[index-1]
            index += 1
        return max_profit

input_list = [3, 1, 4, 2, 5, 6]
# input_list = [3, 1]
s = BestTimeToBuyAndSellStockII()
print s.maxProfit(input_list)
