# coding UTF-8

__author__ = 'LiMingji'


class BestTimeToBuyAndSellStock(object):
    def maxProfit(self, prices):
        if len(prices) == 0:
            return 0
        dp = [0]
        min_in_list = prices[0]
        index = 1
        while index < len(prices):
            dp.append(max(prices[index] - min_in_list, dp[index-1]))
            min_in_list = min(min_in_list, prices[index])
            index += 1
        return dp[len(prices)-1]


input_list = [3, 1, 4, 2, 5, 6]
# input_list = [3, 1]
s = BestTimeToBuyAndSellStock()
print s.maxProfit(input_list)
