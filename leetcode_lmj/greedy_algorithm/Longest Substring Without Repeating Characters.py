# coding: UTF-8
__author__ = 'LiMingji'


class LongestSubstringWithoutRepeatingCharacters(object):
    def lengthOfLongestSubstring(self, s):
        index_map = {}
        index = 0
        total_max = 0
        cur_len = 0
        while index < len(s):
            if s[index] in index_map:
                total_max = max(total_max, cur_len)
                cur_len = index - index_map[s[index]]
                i = index_map[s[index]] + 1
                index_map.clear()
                while i <= index:
                    index_map[s[i]] = i
                    i += 1
            else:
                cur_len += 1
                index_map[s[index]] = index
            index += 1
        total_max = max(total_max, cur_len)
        return total_max


input_str = 'deabcabcbb'
input_str = 'c'
classS = LongestSubstringWithoutRepeatingCharacters()
print classS.lengthOfLongestSubstring(input_str)





