__author__ = 'LiMingji'


class ContainerWithMostWater(object):
    def maxArea(self, height):
        left = 0
        right = len(height) - 1
        max_area = 0
        while left < right:
            area = min(height[left], height[right]) * (right - left)
            max_area = max(max_area, area)
            if height[left] > height[right]:
                right -= 1
            else:
                left += 1
        return max_area


input_nums = [2, 3, 1, 1, 4]
s = ContainerWithMostWater()
print s.maxArea(input_nums)


