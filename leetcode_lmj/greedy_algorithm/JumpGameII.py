__author__ = 'LiMingji'


class JumpGameII(object):
    def jump(self, nums):
        ret = 0
        last = 0
        cur = 0
        for i in range(0, len(nums)):
            if i > last:
                last = cur
                ret += 1
            cur = max(last, i + nums[i])
        return ret

input_nums = [2, 3, 1, 1, 4]
# input_nums = [3, 2, 1, 0, 4]
# input_nums = [2, 1]
# input_nums = [0, 1]

s = JumpGameII()
print s.jump(input_nums)



